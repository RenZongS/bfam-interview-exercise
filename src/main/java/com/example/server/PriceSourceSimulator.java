package com.example.server;

import java.util.Random;

import com.example.marketmaker.ReferencePriceSource;
import com.example.marketmaker.ReferencePriceSourceListener;

/*
 * Simulator to provide price update
 * Trigger update in the data store every second
 */
public class PriceSourceSimulator implements Runnable,ReferencePriceSource {
	private final int[] securityIdList = {5,700,857,1211,9988};
	private final int UPDATE_INTERVAL_MILLIS = 500;
	
	private ReferencePriceSourceListener priceSourceListener;
	private Random random;
	
	public PriceSourceSimulator() {
		super();
		this.random = new Random();
	}
	@Override
	public void run() {
		while (true) {
			int updateIdx = random.nextInt(securityIdList.length);
			priceSourceListener.referencePriceChanged(updateIdx, getRdnPrice());		
			try {
				Thread.sleep(UPDATE_INTERVAL_MILLIS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private double getRdnPrice() {
		return random.nextInt(100) + random.nextDouble();
	}
	@Override
	public void subscribe(ReferencePriceSourceListener listener) {
		this.priceSourceListener = listener;
	}
	/*
	 * Manually set of price into the dataSource
	 */
	public void initializeData() {
		if (priceSourceListener != null) {
			for (int id:securityIdList) {
				priceSourceListener.referencePriceChanged(id,getRdnPrice());
			}
		}
	}

	
}
