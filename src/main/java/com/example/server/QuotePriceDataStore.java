package com.example.server;

import java.util.HashMap;

import com.example.marketmaker.QuoteCalculationEngine;
import com.example.marketmaker.ReferencePriceSourceListener;

/*
 * Local data store Subscribe to the price change event
 * Then update local copy of the reference price
 */
public class QuotePriceDataStore implements QuoteCalculationEngine,ReferencePriceSourceListener{
	// local copy of the most updated reference price for each security
	private volatile HashMap<Integer,Double> securityPriceMap;
	
	public QuotePriceDataStore() {
		this.securityPriceMap = new HashMap<>();
	}
	
	
	/**
	 * Simple implementation to calculate quotePrice,
	 * higher quote for buy, lower quote for sell, 
	 * returns -1 if the security doesn't exist, to indicate invalid
	 */
	@Override
	public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
		if (quantity <= 0) {
			return -1;
		} else if (securityPriceMap.containsKey(securityId)) {
			return securityPriceMap.get(securityId) * quantity * (buy?1.005:0.995);
		} else {
			return -1;
		}
	}

	@Override
	public void referencePriceChanged(int securityId, double price) {
		securityPriceMap.put(securityId, price);
	}
}
