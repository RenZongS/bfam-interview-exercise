package com.example.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Logger;

import com.example.marketmaker.QuoteCalculationEngine;

/*
 * The runnable handling client request
 */
public class GetPriceHandler implements Runnable{
	private Socket socket;
	private QuoteCalculationEngine dataSource;
	private Logger logger = Logger.getLogger(this.getClass().getSimpleName());
	
	public GetPriceHandler(Socket socket,QuoteCalculationEngine dataSource) {
		super();
		this.socket = socket;
		this.dataSource = dataSource;
	}
	@Override
	public void run() {
        try {
        	PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
        	BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        	
        	String responseString = calculatePriceFromRequest(input.readLine()); 

        	// return the price
            output.print(responseString);
            output.close();
            input.close();
            logger.info("Request processed: " + responseString);
        } catch (Exception e) {
            e.printStackTrace();
        }
		
	}
	/*
	 * Parse the client request
	 * utilize QuoteCalculationEngine to calculate the quote price
	 * Return quote price if the request is valid
	 * Else return error information
	 */
	public String calculatePriceFromRequest(String req) {
		req = req.trim();
		logger.info("Calculating price: " + req);
		String [] parts = req.split("\\s+");
		int security;
		boolean isBuy;
		int quantity;
		if (parts.length == 3) {
			try {
				security = Integer.valueOf(parts[0]);
				isBuy = "BUY".equals(parts[1])? true:false;
				quantity = Integer.valueOf(parts[2]);
			} catch(NumberFormatException ex) {
				ex.printStackTrace();
				return "Invalid format for request";
			}
			double quotePrice = dataSource.calculateQuotePrice(security, 0, isBuy, quantity);
			if (quotePrice>0) {
				return String.format("%.4f", quotePrice);
			} else {
				return "Security price is not available";
			}
		} else {
			return "Invalid format for request";
		}
	}
	

}
