package com.example.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;
/**
 * Main Class running the multi-threaded tcp server,
 * Accepting tcp connection in SERVER_PORT for the defined request format
 * return quote price or extra information for invalid request
 */
public class MultiThreadPriceServer{
	
	private final int SERVER_PORT = 12345;
	
	private final Logger logger = Logger.getLogger(this.getClass().getSimpleName());
	
	private ServerSocket serverSocket;
	private Socket clientSocket;
	
	private QuotePriceDataStore dataSource; 
	// simulating price update event
	private PriceSourceSimulator simulatorRunnable; 
	
	private MultiThreadPriceServer() {
		dataSource = new QuotePriceDataStore();
		
		simulatorRunnable = new PriceSourceSimulator();
		simulatorRunnable.subscribe(dataSource);
		simulatorRunnable.initializeData();
		
		(new Thread(simulatorRunnable)).start();
		try {
			serverSocket = new ServerSocket(SERVER_PORT);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void run() {
		logger.info("Server is running");
		while(true) {
			try {
				clientSocket = serverSocket.accept();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			GetPriceHandler priceRunnable = new GetPriceHandler(clientSocket,dataSource);
			(new Thread(priceRunnable)).start();
		}
	}
	
	public static void main(String[] args) {
		MultiThreadPriceServer server = new MultiThreadPriceServer();
			
		server.run();
	}
}
