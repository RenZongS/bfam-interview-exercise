package com.example.test;

import com.example.server.GetPriceHandler;
import com.example.server.QuotePriceDataStore;

public class TestQuotePriceDataStore {
	private QuotePriceDataStore dataStore;
	private GetPriceHandler priceHandler;
	private final double EPSILON = 1e-5;

	public TestQuotePriceDataStore() {
		dataStore = new QuotePriceDataStore();
		priceHandler = new GetPriceHandler(null, dataStore);
		
		dataStore.referencePriceChanged(700, 100.0);
	}
	 
	
	public void testDataStore() {
		// 100 * 100 * 1.005
		assertEquals(true, Math.abs(10050 - dataStore.calculateQuotePrice(700, 0, true, 100)) < EPSILON);
		// 100 * 100 * 0.995
		assertEquals(true, Math.abs(9950 - dataStore.calculateQuotePrice(700, 0, false, 100)) < EPSILON);
		// non existing instrument
		assertEquals(-1.0, dataStore.calculateQuotePrice(701, 0, true, 100));
		// invalid quantity
		assertEquals(-1.0, dataStore.calculateQuotePrice(702, 0, true, -100));
	}
	
	public void testPriceHandler() {
		assertEquals("10050.0000",priceHandler.calculatePriceFromRequest("700 BUY   100  "));
		
		assertEquals("9950.0000",priceHandler.calculatePriceFromRequest("700 SELL 100"));
		
		assertEquals("Invalid format for request", priceHandler.calculatePriceFromRequest("700 SELL 100 a"));
		
		assertEquals("Security price is not available", priceHandler.calculatePriceFromRequest("701 SELL 100"));
		
		assertEquals("Invalid format for request", priceHandler.calculatePriceFromRequest("700 SELL 100.5"));
		
	}
	
	public void assertEquals(Object a, Object b) {
		if (a == null || b == null) {
			if (a != null || b != null) {
				throw new AssertionError(a + " is not equals to " + b);
			} else {
				return;
			}
		} else if (!a.equals(b)){
			throw new AssertionError(a + " is not equals to " + b);
		}
	}
	
	
	public static void main(String [] args) {
		TestQuotePriceDataStore main = new TestQuotePriceDataStore();
		main.testDataStore();
		main.testPriceHandler();
		
		System.out.println("The test is completed without any unexpectation");
	}

}
