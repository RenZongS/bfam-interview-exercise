package com.example.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

public class TestGetPriceClient {

	private static final int REQUEST_INTERVAL_MILLIS = 500;
	private final int SERVER_PORT = 12345;
	private final int[] securityIdList = {5,700,857,1211,9988};
	
	private Random random = new Random();
	
	public void makePriceReq() {
		String request = getNextRequestString();
		
		OutputStream output = null;
		InputStream input = null;
		Socket socket = null;
		try {
			socket = new Socket("127.0.0.1",SERVER_PORT);
			
			output = socket.getOutputStream();
			input = socket.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			PrintWriter writer = new PrintWriter(output, true);
			
			System.out.println("Making request: " + request);
			// send a request
			writer.println(request);
			
			// get the response
			String line = reader.readLine();    
			System.out.println("The response from server: " + line);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				output.close();
				input.close();
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TestGetPriceClient client = new TestGetPriceClient();
		while (true) {
			(new Thread(()-> client.makePriceReq())).run();
			try {
				Thread.sleep(REQUEST_INTERVAL_MILLIS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		}
	// get next random request string to the server
	private String getNextRequestString() {
		int nextIdx = random.nextInt(securityIdList.length);
		return String.format("%d %s %d",securityIdList[nextIdx], random.nextBoolean()?"BUY":"SELL", random.nextInt(100));	
	}

}
