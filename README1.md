## Overview
Tested in Java 1.8, Able to run without any external dependencies

Start up the multi-threaded TCP server

When receiving client request with expected format,

The service calculate the quote price and respond without blocking

While the data store subscribe from price data source, and update the local copy of price

## Files
* `MultiThreadPriceServer.java`: Run this class as the main tcp server, maintaining the local data store, accepting client connection and returns quote price or extra information for invalid request

* `QuotePriceDataStore.java`: Local data store that contains updated reference price. Subscribe to the price change event, provide simple method to calculate quote price from local reference price

* `GetPriceHandler.java`: Handling client request, call the quote price server, and respond to the client

* `PriceSourceSimulator.java`: Simulating periodic price update event

## Testing

* `TestGetPriceClient.java`: run this class after starting the server, it will generate valid request periodically, and print out request/response to the local server

* `TestQuotePriceDataStore.java`: run this class to unit test the functionalities provided by `QuotePriceDataStore.java` and `GetPriceHandler.java`,
Should test the valid case, and basic validation of the classes